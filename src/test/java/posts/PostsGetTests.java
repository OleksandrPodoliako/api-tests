package posts;

import apiclients.PostsClient;
import apiwrappers.RequestWrapper;
import apiwrappers.ResponseWrapper;
import models.Post;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.HashMap;
import java.util.Map;

import static utils.EnvProperties.baseURL;
import static utils.JsonSchemaValidator.isJsonSchemaValid;
import static utils.ObjectToStringJSONConverter.convertToStringJSON;

public class PostsGetTests {
    private static final String URL = baseURL() + "posts";
    private PostsClient postsClient;
    private SoftAssert softAsserts;
    private static final Map<String, String> HEADERS = new HashMap<>() {{
        put("Content-type", "application/json; charset=UTF-8");
    }};
    private ResponseWrapper<Post> responseWrapperPreconditions;

    @BeforeMethod
    public void init() {
        postsClient = new PostsClient();
        softAsserts = new SoftAssert();

        Post post = Post.builder()
                .userId(1)
                .title("title")
                .body("body")
                .build();

        RequestWrapper<Post> requestWrapper = RequestWrapper.<Post>builder()
                .headers(HEADERS)
                .body(post)
                .build();

        responseWrapperPreconditions = postsClient.postEntity(Post.class, requestWrapper, URL);
    }


    @Test
    public void testGetPosts() {
        RequestWrapper<Post> requestWrapper = RequestWrapper.<Post>builder()
                .headers(HEADERS)
                .build();


        ResponseWrapper<Post> responseWrapper = postsClient.getEntity(Post.class, requestWrapper
                , URL + "/" + responseWrapperPreconditions.getBody().getId());

        softAsserts.assertEquals(responseWrapper.getStatusCode(), 200
                , "Response status code should be 200");

        softAsserts.assertTrue((isJsonSchemaValid("postsGetSchema.json",
                        convertToStringJSON(responseWrapper.getBody())))
                , "The search returns response with invalid json schema");

        softAsserts.assertEquals(responseWrapper.getBody().getId(), responseWrapperPreconditions.getBody().getId()
                , "Response id should be correct");
        softAsserts.assertEquals(responseWrapper.getBody().getUserId(), responseWrapperPreconditions.getBody().getUserId()
                , "Response userId should be correct");
        softAsserts.assertEquals(responseWrapper.getBody().getTitle(), responseWrapperPreconditions.getBody().getTitle()
                , "Response title should be correct");
        softAsserts.assertEquals(responseWrapper.getBody().getBody(), responseWrapperPreconditions.getBody().getBody()
                , "Response body should be correct");
        softAsserts.assertAll();
    }

    @AfterMethod
    public void cleanup() {
        RequestWrapper<Post> requestWrapper = RequestWrapper.<Post>builder()
                .headers(HEADERS)
                .body(responseWrapperPreconditions.getBody())
                .build();

        postsClient.deleteEntity(Post.class, requestWrapper
                , URL + "/" + responseWrapperPreconditions.getBody().getId());
    }
}
