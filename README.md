# api-tests

## Free fake API for testing
* https://jsonplaceholder.typicode.com/

## Getting started

install JDK
* download jdk (sapmachine-11.0.10)
* set jdk path to 'JAVA_HOME' environment variable
* add %JAVA_HOME%\bin to 'Path' environment variable

install Apache Maven
* download maven (3.6.3)
* unzip
* set maven path to 'M2_HOME' environment variable
* add %M2_HOME%\bin to 'Path' environment variable

## How to run
- locally - mvn clean test(Report - target/surefire-reports/index.html/)

## Assumptions
* The system really create and delete entities(right now the system just pretend that it changes states. Because of that 
the test is failed)
* The state of system can be managed
* Nobody changing the state of system during test run.
